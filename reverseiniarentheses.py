import unittest


def reverseInParentheses(inputString):
	# TODO


class HMDTest(unittest.TestCase):

	def test_case_1(self):
		self.assertEqual(reverseInParentheses('(bar)'), 'rab')
   

	def test_case_2(self):
		self.assertEqual(reverseInParentheses('foo(bar)baz'), 'foorabbaz')


	def test_case_3(self):
		self.assertEqual(reverseInParentheses('foo(bar)baz(blim)'), 'foorabbazmilb')


	def test_case_4(self):
		self.assertEqual(reverseInParentheses('foo(bar(baz))blim'), 'foobazrabblim')


	def test_case_5(self):
		self.assertEqual(reverseInParentheses(''), '')


	def test_case_6(self):
		self.assertEqual(reverseInParentheses('()'), '')


	def test_case_7(self):
		self.assertEqual(reverseInParentheses('(abc)d(efg)'), 'cbadgfe')


if __name__ == '__main__':
    unittest.main(verbosity=2)
