import unittest


def checkPalindrome(inputString):
	# TODO


class HMDTest(unittest.TestCase):

	def test_case_1(self):
		self.assertTrue(checkPalindrome('aabaa'))
   

	def test_case_2(self):
		self.assertFalse(checkPalindrome('abac'))


	def test_case_3(self):
		self.assertTrue(checkPalindrome('a'))


	def test_case_4(self):
		self.assertFalse(checkPalindrome('az'))


	def test_case_5(self):
		self.assertTrue(checkPalindrome('abacaba'))


	def test_case_6(self):
		self.assertFalse(checkPalindrome('aaabaaaa'))


	def test_case_7(self):
		self.assertFalse(checkPalindrome('zzzazzazz'))

	
	def test_case_8(self):
		self.assertTrue(checkPalindrome('hlbeeykoqqqqokyeeblh'))


if __name__ == '__main__':
    unittest.main(verbosity=2)
