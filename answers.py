import re


def checkPalindrome(inputString):
	return inputString == inputString[::-1]


def shapeArea(n):
    return n**2 + (n-1)**2


def reverseInParentheses(inputString):
    prog = re.compile(r"\(\w*\)")
    
    match = re.search(prog, inputString)
    while match:
        s = match.start()
        e = match.end()
    
        inputString = inputString[0:s] + inputString[s+1:e-1][::-1] + inputString[e:]
        match = re.search(prog, inputString)
        
    return inputString


def mainCross(cm1, cm2):
    return (ord(cm1[0]) - ord(cm1[1])) == (ord(cm2[0]) - ord(cm2[1]))


def subCross(cm1, cm2):
    return (ord(cm1[0]) + ord(cm1[1])) == (ord(cm2[0]) + ord(cm2[1]))
    
    
def bishopAndPawn(bishop, pawn):
    return mainCross(bishop, pawn) or subCross(bishop, pawn) 
