import unittest


def shapeArea(n):
	# TODO



class HMDTest(unittest.TestCase):

	def test_case_1(self):
		self.assertEqual(shapeArea(2), 5)
   

	def test_case_2(self):
		self.assertEqual(shapeArea(3), 13)


	def test_case_3(self):
		self.assertEqual(shapeArea(1), 1)


	def test_case_4(self):
		self.assertEqual(shapeArea(5), 41)


	def test_case_5(self):
		self.assertEqual(shapeArea(7000), 97986001)


	def test_case_6(self):
		self.assertEqual(shapeArea(8000), 127984001)


	def test_case_7(self):
		self.assertEqual(shapeArea(8999), 161946005)

	
	def test_case_8(self):
		self.assertEqual(shapeArea(100), 19801)


if __name__ == '__main__':
    unittest.main(verbosity=2)
