import unittest


def bishopAndPawn(bishop, pawn):
    # TODO


class HMDTest(unittest.TestCase):

    def test_case_1(self):
        self.assertTrue(bishopAndPawn('a1', 'c3'))
   

    def test_case_2(self):
        self.assertFalse(bishopAndPawn('h1', 'h3'))


    def test_case_3(self):
        self.assertTrue(bishopAndPawn('a5', 'c3'))


    def test_case_4(self):
        self.assertFalse(bishopAndPawn('g1', 'f3'))


    def test_case_5(self):
        self.assertTrue(bishopAndPawn('e7', 'd6'))


    def test_case_6(self):
        self.assertTrue(bishopAndPawn('e7', 'a3'))


    def test_case_7(self):
        self.assertTrue(bishopAndPawn('e3', 'a7'))

    
    def test_case_8(self):
        self.assertTrue(bishopAndPawn('a1', 'h8'))


    def test_case_9(self):
        self.assertFalse(bishopAndPawn('a1', 'h7'))


    def test_case_10(self):
        self.assertTrue(bishopAndPawn('h1', 'a8'))


if __name__ == '__main__':
    unittest.main(verbosity=2)